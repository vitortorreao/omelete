# -*- coding: utf-8 -*-

"""Downloads the documents from the web"""

import datetime
import fileinput
import json
import requests

from bs4 import BeautifulSoup


HTML_PARSER = "html.parser"  # used by beautifulsoup4
OMELETE_URL = "https://omelete.uol.com.br"

JSON_FILE = open("documents.json", "w", encoding="utf-8")
for sufix in fileinput.input():
    sufix = sufix[:-1]  # remove the \n in the end
    response = requests.get(OMELETE_URL + sufix)
    document = BeautifulSoup(response.text, HTML_PARSER)
    title = document.find("h1", {"class": "title", "itemprop": "name"}) \
                    .getText().strip()
    print(title)
    description = document.find("div", {"class": "subtitle"}) \
                          .getText().strip()
    date = datetime.datetime.strptime(
        document.find("span", {"itemprop":"datePublished"}).getText().strip(),
        "%d/%m/%Y - %H:%M")
    author = document.find("span", {"itemprop": "author"}) \
                     .find("span", {"itemprop": "name"}) \
                     .getText() \
                     .strip()
    raw_content = document.find("div", {"class": "content content-body"})
    paragraphs = list()
    for paragraph in raw_content.find_all("p")[:-1]:
        paragraphs.append(paragraph.getText())
    try:
        score = int(document.find("span", {"class": "nota-critica",
                                           "itemprop": "ratingValue"})
                    ["content"])
    except Exception:
        score = -1
    obj = {
        "title": title,
        "subtitle": description,
        "date": date.isoformat(),
        "author": author,
        "paragraphs": paragraphs,
        "score": score
    }
    JSON_FILE.write(json.dumps(obj, ensure_ascii=False))
    JSON_FILE.write("\n")
JSON_FILE.close()
