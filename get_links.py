"""Downloads the links to the documents from the web"""

import requests

from bs4 import BeautifulSoup


HTML_PARSER = "html.parser"  # used by beautifulsoup4

OMELETE_URL = "https://omelete.uol.com.br"
OMELETE_DB_SUFIX = "/filmes/critica/"
PAGE_ARG = "?pagina={0}"

RESPONSE = requests.get(OMELETE_URL + OMELETE_DB_SUFIX)

DOCUMENT = BeautifulSoup(RESPONSE.text, HTML_PARSER)

LAST_PAGE = int(DOCUMENT.find_all("ul", {"class": "paginator"})[0]
                .find_all("li")[-2]["data-value"])

for page_num in range(1, LAST_PAGE + 1):  # range goes from start to end - 1
    url = (OMELETE_URL + OMELETE_DB_SUFIX + PAGE_ARG).format(page_num)
    response = requests.get(url)
    document = BeautifulSoup(response.text, HTML_PARSER)
    details = document.find_all("div", {"class": "details"})
    for detail in details:
        link = detail.find_all("div", {"class": "call"})[0].find_all("a")[0]
        print(link["href"])
